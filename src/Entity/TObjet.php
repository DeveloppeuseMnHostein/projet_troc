<?php

namespace App\Entity;

use App\Entity\TCategories;
use App\Entity\TUtilisateurs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TObjet
 *
 * @ORM\Table(name="t_objet", indexes={@ORM\Index(name="fk_category_idx1", columns={"fk_category"}), @ORM\Index(name="fk_category_idx", columns={"id"}), @ORM\Index(name="fk_user_id_idx", columns={"fk_user_id"})})
 * @ORM\Entity
 */
class TObjet
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var string
     *
     *  * @Assert\Length(
     *      min = 2,
     *      max = 200,
     *      minMessage = "The name must be at least {{ limit }} characters long",
     *      maxMessage = "The name cannot be longer than {{ limit }} characters"
     * )
     * 
     * @ORM\Column(name="name", type="string", length=200, nullable=false, options={"comment"="lenght >1"})
     */
    private string $name;

    /**
     * @var string
     *
     *  * @Assert\Length(
     *      min = 11,
     *      max = 65535,
     *      minMessage = "The description must be at least {{ limit }} characters long",
     *      maxMessage = "The description cannot be longer than {{ limit }} characters"
     * )
     * 
     * @ORM\Column(name="description", type="text", length=65535, nullable=false, options={"comment"="lenght>10"})
     */
    private string $description;

    /**
     * @var string
     * @Assert\Length(
     *      min = 16,
     *      max = 255,
     *      minMessage = "The url must be at least {{ limit }} characters long",
     *      maxMessage = "The url cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(name="illustration", type="string", length=255, nullable=false, options={"comment"="lenght>15"})
     */
    private string $illustration;

    /**
     * @var \TCategories
     *
     * @ORM\ManyToOne(targetEntity="TCategories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_category", referencedColumnName="id")
     * })
     */
    private $fkCategory;

    /**
     * @var \TUtilisateurs
     *
     * @ORM\ManyToOne(targetEntity="TUtilisateurs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_user_id", referencedColumnName="user_id")
     * })
     */
    private $fkUser;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="TMessage", inversedBy="idObjet")
     * @ORM\JoinTable(name="t_message_objets",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_objet", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_message", referencedColumnName="message_id")
     *   }
     * )
     */
    private $idMessage;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idMessage = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }


    public function setName(string $name): self
    {
        $this->name = filter_var($name, FILTER_SANITIZE_STRING);

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = filter_var($description, FILTER_SANITIZE_STRING);

        return $this;
    }

    public function getIllustration(): ?string
    {
        return $this->illustration;
    }

    public function setIllustration(string $illustration): self
    {
        $this->illustration = filter_var($illustration, FILTER_SANITIZE_STRING);

        return $this;
    }

    public function getFkCategory(): ?TCategories
    {
        return $this->fkCategory;
    }

    public function setFkCategory(?TCategories $fkCategory): self
    {
        $this->fkCategory = $fkCategory;

        return $this;
    }

    public function getFkUser(): ?TUtilisateurs
    {
        return $this->fkUser;
    }

    public function setFkUser(?TUtilisateurs $fkUser): self
    {
        $this->fkUser = $fkUser;

        return $this;
    }

    /**
     * @return Collection|TMessage[]
     */
    public function getIdMessage(): Collection
    {
        return $this->idMessage;
    }

    public function addIdMessage(TMessage $idMessage): self
    {
        if (!$this->idMessage->contains($idMessage)) {
            $this->idMessage[] = $idMessage;
        }

        return $this;
    }

    public function removeIdMessage(TMessage $idMessage): self
    {
        $this->idMessage->removeElement($idMessage);

        return $this;
    }
}
