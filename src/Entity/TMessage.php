<?php

namespace App\Entity;

use DateTime;
use App\Entity\TStatut;
use App\Entity\TUtilisateurs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * TMessage
 *
 * @ORM\Table(
 *  name="t_message", 
 *  indexes={
 *      @ORM\Index(
 *name="fk_user_id_idx", 
 *columns={"destinataire"}), 
 *  @ORM\Index(
 * name="fk_emetteur_idx", 
 * columns={"emetteur"}), 
 *  @ORM\Index(
 * name="fk_id_statut_idx", 
 * columns={"statut"})})
 * @ORM\Entity
 */
class TMessage
{
    /**
     * @var int
     *
     * @ORM\Column(name="message_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * 
     * @Assert\GreaterThan(0)
     */
    private ?int $messageId;

    /**
     * @var string
     *
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "The object must be at least {{ limit }} characters long",
     *      maxMessage = "The object cannot be longer than {{ limit }} characters"
     * )
     * 
     * @ORM\Column(name="objet", type="string", length=255, nullable=false, options={"comment"=">1"})
     */
    private $objet;

    /**
     * @var string
     * @Assert\Length(
     * min= 2,
     * max= 65635,
     *      minMessage = "Your content must be at least {{ limit }} characters long",
     *      maxMessage = "Your content cannot be longer than {{ limit }} characters"
     * )
     *
     * @ORM\Column(name="corps", type="text", length=65535, nullable=false, options={"comment"="length>1"})
     */
    private string $corps;

    /**
     * @var \DateTime
     *
     * @Assert\Type("\DateTime")
     * 
     * @ORM\Column(name="date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private \DateTime $date;

    /**
     * @var string
     * 
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "The attachment name must be at least {{ limit }} characters long",
     *      maxMessage = "The attachment name cannot be longer than {{ limit }} characters"
     * )
     * 
     * @Assert\Url
     * 
     *
     * @ORM\Column(name="piece_jointe", type="string", length=50, nullable=false, options={"comment"="lenght>1"})
     */
    private $pieceJointe;

    /**
     * @Assert\File(
     * maxSize = "5000k",
     * mimeTypes = {"application/pdf", "application/x-pdf", "application/word",},
     * mimeTypesMessage = "Please upload a valid file"
     * )
     * 
     */
    private $fichier;

    /**
     * @var \TUtilisateurs
     *
     * @Assert\Type("App\Entity\TUtilisateurs")
     * 
     * @ORM\ManyToOne(targetEntity="TUtilisateurs")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="destinataire", referencedColumnName="user_id")
     * })
     */
    private $destinataire;

    /**
     * @var \TStatut
     *
     * 
     * @Assert\Type("App\Entity\TStatut")
     * @ORM\ManyToOne(targetEntity="TStatut")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statut", referencedColumnName="id")
     * })
     */
    private $statut;

    /**
     * @var \TUtilisateurs
     *
     *  @Assert\Type("App\Entity\TUtilisateurs")
     * 
     * @ORM\ManyToOne(targetEntity="TUtilisateurs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="emetteur", referencedColumnName="user_id")
     * })
     */
    private $emetteur;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="TObjet", mappedBy="idMessage")
     */
    private $idObjet;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idObjet = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getMessageId(): ?int
    {
        return $this->messageId;
    }

    public function getObjet(): ?string
    {
        return $this->objet;
    }

    public function setObjet(string $objet): self
    {
        $this->objet = filter_var($objet, FILTER_SANITIZE_STRING);

        return $this;
    }

    public function getCorps(): ?string
    {
        return $this->corps;
    }

    public function setCorps(string $corps): self
    {
        $this->corps = filter_var($corps, FILTER_SANITIZE_STRING);

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPieceJointe(): ?string
    {
        return $this->pieceJointe;
    }

    public function setPieceJointe(string $pieceJointe): self
    {
        $this->pieceJointe = filter_var($pieceJointe, FILTER_SANITIZE_STRING);

        return $this;
    }

    public function getDestinataire(): ?TUtilisateurs
    {
        return $this->destinataire;
    }

    public function setDestinataire(?TUtilisateurs $destinataire): self
    {
        $this->destinataire = $destinataire;

        return $this;
    }

    public function getStatut(): ?TStatut
    {
        return $this->statut;
    }

    public function setStatut(?TStatut $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getEmetteur(): ?TUtilisateurs
    {
        return $this->emetteur;
    }

    public function setEmetteur(?TUtilisateurs $emetteur): self
    {
        $this->emetteur = $emetteur;

        return $this;
    }

    /**
     * @return Collection|TObjet[]
     */
    public function getIdObjet(): Collection
    {
        return $this->idObjet;
    }

    public function addIdObjet(TObjet $idObjet): self
    {
        if (!$this->idObjet->contains($idObjet)) {
            $this->idObjet[] = $idObjet;
            $idObjet->addIdMessage($this);
        }

        return $this;
    }

    public function removeIdObjet(TObjet $idObjet): self
    {
        if ($this->idObjet->removeElement($idObjet)) {
            $idObjet->removeIdMessage($this);
        }

        return $this;
    }
}
