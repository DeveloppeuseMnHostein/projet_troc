<?php

namespace App\Entity;

use App\Entity\TKeys;
use App\Entity\TObjet;
// use Webmozart\Assert\Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TCaracteristiquesObjet
 *
 * @ORM\Table(
 *      name="t_caracteristiques_objet", 
 *      indexes={@ORM\Index(name="fk_key_idx", columns={"key"}),
 * @ORM\Index(name="fk_objet_id_idx", columns={"object_id"})})
 * @ORM\Entity
 */
class TCaracteristiquesObjet
{
    /**
     * @var int
     *
     * @ORM\Column(name="value", 
     *      type="integer", 
     *      nullable=false, 
     *      options={"comment"="lenght>0"})
     * 
     * @Assert\GreaterThan(0)
     */
    private int $value;

    /**
     * @var \TKeys
     *
     * @Assert\Type("App\Entity\TKeys")
     * 
     * @ORM\ManyToOne(targetEntity="TKeys")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="key", referencedColumnName="keys_id")
     * })
     */
    private $key;

    /**
     * @var \TObjet
     *
     *  @Assert\Type("App\Entity\TObjet")
     * 
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="TObjet")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="object_id", referencedColumnName="id")
     * })
     */
    private $object;

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getKey(): ?TKeys
    {
        return $this->key;
    }

    public function setKey(?TKeys $key): self
    {
        $this->key = $key;

        return $this;
    }

    public function getObject(): ?TObjet
    {
        return $this->object;
    }

    public function setObject(?TObjet $object): self
    {
        $this->object = $object;

        return $this;
    }
}
