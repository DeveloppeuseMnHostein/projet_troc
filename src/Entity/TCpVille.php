<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TCpVille
 *
 * @ORM\Table(name="t_cp_ville")
 * @ORM\Entity
 */
class TCpVille
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_cp_town", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $idCpTown;

    /**
     * @var string
     * 
     * @Assert\Regex(
     *     pattern = "/^(?!0{2})\d{5}$/",
     *     message = "postal code invalid"
     * )
     *
     * @ORM\Column(name="postal_code", type="string", length=5, nullable=false, options={"comment"="lenght=5 ('0'<=char<='9')"})
     */
    private ?string $postalCode;

    /**
     * @var string
     *
     * * @Assert\Length(
     *      min = 1,
     *      max = 120,
     *      minMessage = "The name of the town must be at least {{ limit }} characters long",
     *      maxMessage = "The name of the town cannot be longer than {{ limit }} characters"
     * )
     * 
     * @ORM\Column(name="town", type="string", length=120, nullable=false, options={"comment"=">0"})
     */
    private string $town;

    public function getIdCpTown(): ?int
    {
        return $this->idCpTown;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getTown(): ?string
    {
        return $this->town;
    }

    public function setTown(string $town): self
    {
        $this->town = filter_var($town, FILTER_SANITIZE_STRING);

        return $this;
    }
}
