<?php

namespace App\Entity;

use App\Entity\TCpVille;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\TCompteUtilisateur;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TUtilisateurs
 *
 * @ORM\Table
 * (name="t_utilisateurs",
 *  indexes={@ORM\Index
 * (name="fk_cp_town_idx", 
 * columns={"fk_cp_town"})
 * })
 * @ORM\Entity
 */
class TUtilisateurs
{


    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id;

    /**
     * @var string
     * 
     * @Assert\Length(
     *      min = 1,
     *      max = 60,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     *)
     * @ORM\Column(
     * name="firstname",
     *  type="string", 
     * length=60, 
     * nullable=false, 
     * options={"comment"="'>1 characters'"}
     * )
     */
    private $firstname;

    /**
     * @var string
     *
     *  * @Assert\Length(
     *      min = 1,
     *      max = 60,
     *      minMessage = "Your last name must be at least {{ limit }} characters long",
     *      maxMessage = "Your last name cannot be longer than {{ limit }} characters"
     *)
     * 
     * 
     * @ORM\Column(
     * name="lastname",
     * type="string", 
     * length=60, 
     * nullable=false, 
     * options={"comment"="'>1 characters'"}
     * )
     */
    private string $lastname;

    /**
     * @var \DateTime
     *
     * @Assert\Type("\DateTime");
     * @Assert\GreaterThan("16 years")
     * @Assert\LessThan("125 years"),
     * 
     * @Assert\Type("App\Entity\TUtilisateurs")
     * 
     * @ORM\Column(
     * name="birthdate", 
     * type="date", 
     * nullable=false, 
     * options={"comment"="> now -16 ans AND now -125"}
     * )
     */
    private \DateTime $birthdate;

    /**
     * @var string
     *
     *  @Assert\Length(
     *      min = 10,
     *      max = 10,
     *      minMessage = "Your phone number must be at least {{ limit }} characters long",
     *      maxMessage = "Your phone cannot be longer than {{ limit }} characters"
     *)
     *  @Assert\Regex(
     *     pattern="/^(\+33\s[1-9]{8})|(0[1-9]\s{8})$/",
     *     match=false,
     *     message="Your phone number cannot contain a string."
     * )
     * 
     * @ORM\Column(
     * name="phone", 
     * type="string", 
     * length=10, 
     * nullable=false, 
     * options={"comment"="'lenght >= 10 (\'0\' <= char<=\'9\')'"}
     * )
     */
    private string $phone = '';

    /**
     * @var string
     *
     * @Assert\Regex(
     *       pattern="/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)",
     *       match=false,
     *       message="Veuillez saisir une adresse email valide."
     * )
     * 
     * @ORM\Column(
     * name="email", 
     * type="string", 
     * length=320, 
     * nullable=false, 
     * options={"comment"="'lenght >=8 regex email'"}
     * )
     */
    private $email;

    /**
     * @var int
     *
     * @Assert\Greaterthan(0)
     * 
     * @ORM\Column(
     * name="street_number", 
     * type="integer", 
     * nullable=false, 
     * options={"comment"=">0"}
     * )
     */
    private ?int $streetNumber;

    /**
     * @var string
     *
     * @ORM\Column(
     * name="street_name", 
     * type="string", 
     * length=520, 
     * nullable=false, 
     * options={"comment"="'>1'"}
     * )
     */
    private string $streetName = '';

    /**
     * @var \TCpVille
     *
     * @Assert\Greaterthan(0)
     * 
     * @ORM\ManyToOne(targetEntity="TCpVille")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(
     *  name="fk_cp_town", 
     *  referencedColumnName="id_cp_town"
     * )
     * })
     */
    private ?int $fkCpTown;

    /**
     * @var \TCompteUtilisateur
     *
     * 
     * @Assert\Type("App\Entity\TCompteUtilisateur")
     * 
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="TCompteUtilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = filter_var($firstname, FILTER_SANITIZE_STRING);

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = filter_var($lastname, FILTER_SANITIZE_STRING);

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = filter_var($email, FILTER_SANITIZE_STRING);

        return $this;
    }

    public function getStreetNumber(): ?int
    {
        return $this->streetNumber;
    }

    public function setStreetNumber(int $streetNumber): self
    {
        $this->streetNumber = $streetNumber;

        return $this;
    }

    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    public function setStreetName(string $streetName): self
    {
        $this->streetName = filter_var($streetName, FILTER_SANITIZE_STRING);

        return $this;
    }

    public function getFkCpTown(): ?TCpVille
    {
        return $this->fkCpTown;
    }

    public function setFkCpTown(?TCpVille $fkCpTown): self
    {
        $this->fkCpTown = $fkCpTown;

        return $this;
    }

    public function getUser(): ?TCompteUtilisateur
    {
        return $this->user;
    }

    public function setUser(?TCompteUtilisateur $user): self
    {
        $this->user = $user;

        return $this;
    }
}
