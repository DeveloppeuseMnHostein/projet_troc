<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TCompteUtilisateur
 *
 * @ORM\Table(name="t_compte_utilisateur", uniqueConstraints={@ORM\UniqueConstraint(name="login_UNIQUE", columns={"login"})})
 * @ORM\Entity
 */
class TCompteUtilisateur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id;

    /**
     * @var string
     *
     * @Assert\Length(
     *      min = 2,
     *      max = 45,
     *      minMessage = "Your login must be at least {{ limit }} characters long",
     *      maxMessage = "Your login cannot be longer than {{ limit }} characters"
     * )
     * 
     * 
     * @ORM\Column(name="login", type="string", length=45, nullable=false, options={"default"="> 7 characters"})
     */
    private string $login = '';

    /**
     * @var string
     * 
     * @Assert\Regex(
     *     pattern = "/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/",
     *      message = "Your password is not valid."
     * )
     *
     * @ORM\Column(name="password", type="string", length=45, nullable=false, options={"default"=">7 characters AND regex(password)"})
     */
    private ?string $password = '';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = filter_var($login, FILTER_SANITIZE_STRING);

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
}
