<?php

namespace App\Controller;

use App\Entity\TCpVille;
use App\Form\TCpVilleType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/t/cp/ville")
 */
class TCpVilleController extends AbstractController
{
    /**
     * @Route("/", name="t_cp_ville_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $tCpVilles = $entityManager
            ->getRepository(TCpVille::class)
            ->findAll();

        return $this->render('t_cp_ville/index.html.twig', [
            't_cp_villes' => $tCpVilles,
        ]);
    }

    /**
     * @Route("/new", name="t_cp_ville_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $tCpVille = new TCpVille();
        $form = $this->createForm(TCpVilleType::class, $tCpVille);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($tCpVille);
            $entityManager->flush();

            return $this->redirectToRoute('t_cp_ville_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_cp_ville/new.html.twig', [
            't_cp_ville' => $tCpVille,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{idCpTown}", name="t_cp_ville_show", methods={"GET"})
     */
    public function show(TCpVille $tCpVille): Response
    {
        return $this->render('t_cp_ville/show.html.twig', [
            't_cp_ville' => $tCpVille,
        ]);
    }

    /**
     * @Route("/{idCpTown}/edit", name="t_cp_ville_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TCpVille $tCpVille, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TCpVilleType::class, $tCpVille);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('t_cp_ville_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_cp_ville/edit.html.twig', [
            't_cp_ville' => $tCpVille,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{idCpTown}", name="t_cp_ville_delete", methods={"POST"})
     */
    public function delete(Request $request, TCpVille $tCpVille, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tCpVille->getIdCpTown(), $request->request->get('_token'))) {
            $entityManager->remove($tCpVille);
            $entityManager->flush();
        }

        return $this->redirectToRoute('t_cp_ville_index', [], Response::HTTP_SEE_OTHER);
    }
}
