<?php

namespace App\Controller;

use App\Entity\TMessage;
use App\Form\TMessageType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/t/message")
 */
class TMessageController extends AbstractController
{
    /**
     * @Route("/", name="t_message_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $tMessages = $entityManager
            ->getRepository(TMessage::class)
            ->findAll();

        return $this->render('t_message/index.html.twig', [
            't_messages' => $tMessages,
        ]);
    }

    /**
     * @Route("/new", name="t_message_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $tMessage = new TMessage();
        $form = $this->createForm(TMessageType::class, $tMessage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($tMessage);
            $entityManager->flush();

            return $this->redirectToRoute('t_message_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_message/new.html.twig', [
            't_message' => $tMessage,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{messageId}", name="t_message_show", methods={"GET"})
     */
    public function show(TMessage $tMessage): Response
    {
        return $this->render('t_message/show.html.twig', [
            't_message' => $tMessage,
        ]);
    }

    /**
     * @Route("/{messageId}/edit", name="t_message_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TMessage $tMessage, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TMessageType::class, $tMessage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('t_message_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_message/edit.html.twig', [
            't_message' => $tMessage,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{messageId}", name="t_message_delete", methods={"POST"})
     */
    public function delete(Request $request, TMessage $tMessage, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tMessage->getMessageId(), $request->request->get('_token'))) {
            $entityManager->remove($tMessage);
            $entityManager->flush();
        }

        return $this->redirectToRoute('t_message_index', [], Response::HTTP_SEE_OTHER);
    }
}
